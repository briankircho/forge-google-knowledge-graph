import ForgeUI, {
  render,
  Fragment,
  Text,
  Image,
  ContextMenu,
  InlineDialog,
  useProductContext,
  useState,
} from '@forge/ui';
import { fetch } from '@forge/api';

const KNOWLEDGE_GRAPH_API_KEY = '[ADD YOUR API KEY HERE]';
const KNOWLEDGE_GRAPH_API_ENDPOINT = 'https://kgsearch.googleapis.com/v1/entities:search';

export const getKnowledgeGraphResult = async (selectedText) => {
  if (!selectedText) {
    return false;
  }
  return (await fetch(`${KNOWLEDGE_GRAPH_API_ENDPOINT}?${querystring.stringify({
    query: selectedText,
    key: KNOWLEDGE_GRAPH_API_KEY,
    limit: 1,
  })}`)).json();
};

export const KnowledgeGraphDisplay = ({ data, selectedText }) => {
  if (data.error) {
    return <Text content={data.error.message} />;
  }
  const { itemListElement } = data;
  if (!itemListElement.length) {
    return <Text content={`Sorry, no information was found about *${selectedText}*`} format="markdown" />
  }
  const {
    result: {
      name,
      description,
      detailedDescription,
      image,
    },
  } = itemListElement[0];

  const fragments = [];
  let content = '';
  if (name) {
    content += `**${name}**\n`;
  }
  if (description) {
    content += `*${description}*\n`;
  }
  if (detailedDescription) {
    content += detailedDescription.articleBody;
    if (detailedDescription.url) {
      content += ` [Read More](${detailedDescription.url})`;
    }
    content += '\n';
  }
  content += '\n';
  fragments.push(<Text content={content} format="markdown" />);
  if (image && image.contentUrl) {
    fragments.push(<Image src={image.contentUrl} />);
  }
  return (
    <Fragment>
      {fragments}
    </Fragment>
  );
}

const App = () => {
  const { extensionContext: { selectedText } } = useProductContext();
  const [data] = useState(getKnowledgeGraphResult(selectedText));
  if (!selectedText) {
    return null;
  }

  return (
    <InlineDialog>
      <KnowledgeGraphDisplay
        data={data}
        selectedText={selectedText}
      />
    </InlineDialog>
  );
};

export const run = render(
  <ContextMenu>
    <App/>
  </ContextMenu>
);
